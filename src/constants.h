/**
 * Code from the game maze
 * 
 * @Author Alexandre Blondin Massé
 */
// General constants
#define SCREEN_WIDTH  900
#define SCREEN_HEIGHT (900 * 4 / 5)

// Menu constants
#define TITLE_FILENAME "assets/title.png"
#define TITLE_WIDTH 864
#define TITLE_X ((SCREEN_WIDTH - TITLE_WIDTH) / 2)
#define TITLE_Y 0
#define PLAY_FILENAME "assets/play.png"
#define PLAY_WIDTH 400
#define PLAY_X ((SCREEN_WIDTH - PLAY_WIDTH) / 2)
#define PLAY_Y 300
#define QUIT_FILENAME "assets/quit.png"
#define QUIT_WIDTH 400
#define QUIT_X ((SCREEN_WIDTH - QUIT_WIDTH) / 2)
#define QUIT_Y 400

// Level
#define START_POS_X 0
#define START_POS_Y SCREEN_HEIGHT-100

// Character
#define CHARACTER_SPRITESHEET "inf3135-aut2017-tp3-assets/character.png"
#define CHARACTER_SCALE ((SCREEN_WIDTH) / 2560.0)
#define MOVE_DURATION 500
#define CHARACTER_WALKING_RIGHT_ROW 0
#define CHARACTER_WALKING_LEFT_ROW 1
#define CHARACTER_WALKING_DOWN_ROW 2
#define CHARACTER_JUMPING_ROW 3
#define CHARACTER_BETWEEN_FRAME (MOVE_DURATION / 32)
#define CHARACTER_HORIZONTAL_STEP (SCREEN_WIDTH / 15)
#define CHARACTER_VERTICAL_STEP (SCREEN_HEIGHT / 8)
