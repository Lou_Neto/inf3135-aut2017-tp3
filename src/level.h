/**
 * Based on code from the game maze by Alexandre Blondin Massé
 * 
 * @Author Lou-Gomes Neto
 */
#ifndef LEVEL_H
#define LEVEL_H

#include "utils.h"
#include "sdl2.h"
#include <stdbool.h>

// --------------- //
// Data structures //
// --------------- //

struct Level {
    struct Position start;      
    SDL_Texture *texture;   // The texture (image)
    SDL_Renderer *renderer; // The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a level from a file.
 *
 * @param filename  The path of the file
 * @param renderer  The renderer
 * @return          A pointer to the create level, NULL if there was an error;
 *                  Call IMG_GetError() for more information.
 */
struct Level *Level_create(char *filename, SDL_Renderer *renderer);

/**
 * Delete the given level.
 *
 * @param level  The level to be deleted
 */
void Level_delete(struct Level *level);

/**
 * Renders the level with a renderer.
 *
 * @param level      The level to be rendered
 */
void Level_render(struct Level *level);

#endif
