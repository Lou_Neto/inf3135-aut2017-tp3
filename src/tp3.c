/**
 * Code from the game maze by Alexandre Blondin Massé
 * 
 * Creates a game application, runs it and shuts it down once done.
 * 
 * @Author Lou-Gomes Neto
 */
#include "application.h"
#include "sdl2.h"
#include <stdio.h>

int main() {
    struct Application *application = Application_initialize();
    if (application != NULL) {
        Application_run(application);
    } else {
        fprintf(stderr, "Failed to initialize the application...");
        return -1;
    }
    Application_shutDown(application);
}
