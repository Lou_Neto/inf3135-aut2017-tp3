# Travail pratique 3

## Description

Ce projet est un jeu vidéo de platformes où le personnage, Bob, dois manger tous les beignes du niveau.

Ceci est un projet réalisé dans le câdre du cours:
- INF3135: Construction et maintenance de logiciels
- à l'Université du Québec à Montréal 
- durant la session d'automne 2017.

## Auteurs

- Lou-Gomes Neto (NETL14039105)

## Fonctionnement

Une fois l'application lancée, il y a deux choix possibles au menu principal. Il est possible d'appuyer sur `1` pour jouer ou `2` pour quitter le jeu.

Lors de la partie, Bob peut marcher à l'aide des flèches directionnelles `gauche` et `droite`. Il peut aussi sauter avec la touche `haut`.

Pour gagner la partie, il faut que Bob mange tous les beignes du niveau.

## Plateformes supportées

Testé sur Ubuntu 17.10.

## Dépendances

- [SDL2](https://www.libsdl.org/), une bibliothèque de bas niveau pour la conception d'applications graphiques.
- [SDL Image](https://www.libsdl.org/projects/SDL_image/), pour la gestion de chargement d'images.
- [Les ressources graphiques fournies par le professeur](https://gitlab.com/ablondin/inf3135-aut2017-tp3-assets) 

### Ressources graphiques

Les ressources graphiques proviennent du sous module git `inf3135-aut2017-tp3-assets`. Au momment où on clône le projet, le dossier est vide. Pour obtenir les ressources graphiques nécéssaires il faut entrer la commande:

```sh
git submodule init
```

suivi de:

```sh
git submodule update
```

## Compilation

Une fois les dépendances et le sous module installés, le projet peut être compilé au niveau de la racine du projet à l'aide de la commande:

```sh
make
```

Ceci génère un exécutable dans un répertoire `bin`. Pour lancer l'application il suffit d'entrer la commande:


```sh
bin/tp3
```

## Références

- [Le site web du professeur Alexandre Blondin Massé](http://www.lacim.uqam.ca/~blondin/fr/inf3135)
- [Le dépot du jeu Maze, fait par Alexandre Blondin Massé](https://bitbucket.org/ablondin-projects/maze-sdl). Plusieurs modules et segments de codes dans le projet proviennent de cette source.
- [Un site web tutoriel sur la biliothèque SDL](http://lazyfoo.net/tutorials/SDL/)

## Statut

Le projet n'est pas complètement terminé. Le menu principal est fonctionnel et offre deux options: jouer ou quitter. Dans le jeu, seul le personnage est visible. Il est possible de le faire courrir vers la gauche, la droite ainsi que de le faire sauter. La force gravitationnelle ne fonctionne pas donc le personnage ne redescend pas vers le sol. les limites sont établies, donc le personnage ne peut pas depasser les bordures de la fenêtre.